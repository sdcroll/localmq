# Introduction

Package localmq provides an in-process local message bus.

See the [doc.go](doc.go) file for a synopsis.

# Build

To build, run 'make'. This will execute the tests, coverage, and static analysis targets.

# Targets

To see a brief description of the Makefile targers, run 'make help':

```
$ make help
Usage:
  all                execute the tests, coverage, and static analysis targets
  help               print this help message
  test               run the tests with the race detector enabled and produce a code coverage file
  cover/func         print the coverage report
  cover/html         generate the HTML coverage report and open in a browser
  bench/test         run the benchmarks
  bench/cpu          run the benchmarks and print the CPU profile report
  bench/mem          run the benchmarks and print the memory profile report
  doc/pkgsite        run a local pkgsite HTTP server and open in a browser
  go/staticcheck     run 'staticcheck' static analyzer
  go/vet             run the 'go vet' static analyzer
  go/install-tools   install/upgrade the staticcheck and pkgsite tools
  ```
