# ==================================================================================================
# variables

count ?= 1

verbose ?=1
ifeq "$(verbose)" "1"
	v="-v"
else
	v=
endif

# ==================================================================================================
# targets

## all: execute the tests, coverage, and static analysis targets
.PHONY: all
all: test cover/func go/static-analysis

## help: print this help message
# per "Let's Go Further", by Alex Edwards
.PHONY: help
help:
	@echo 'Usage:'
	@sed -n 's/^##//p' ${MAKEFILE_LIST} | column -t -s ':' |  sed -e 's/^/ /'

## test: run the tests with the race detector enabled and produce a code coverage file
.PHONY: test
test:
	go test $(v) -race -count=$(count) \
	  -coverpkg=./... -covermode=atomic -coverprofile=cover.out \
	  ./test

## cover/func: print the coverage report
.PHONY: cover/func
cover/func:
	go tool cover -func=cover.out

## cover/html: generate the HTML coverage report and open in a browser
.PHONY: cover/html
cover/html:
	go tool cover -html=cover.out

## bench/test: run the benchmarks
.PHONY: bench/test
bench/test:
	go test -v -bench=. -run=^# -benchmem ./...

## bench/cpu: run the benchmarks and print the CPU profile report
.PHONY: bench/cpu
bench/cpu:
	go test -v -bench=. -run=^# -benchmem -cpuprofile=cpu.prof ./test
	go tool pprof -text cpu.prof

## bench/mem: run the benchmarks and print the memory profile report
.PHONY: bench/mem
bench/mem:
	go test -v -bench=. -run=^# -benchmem -memprofile=mem.prof ./test
	go tool pprof -text mem.prof

## doc/pkgsite: run a local pkgsite HTTP server and open in a browser
.PHONY: doc/pkgsite
doc/pkgsite:
	$$(go env GOPATH)/bin/pkgsite -http=localhost:6060 -open

## go/static-analysis: run all static analysis targets
.PHONY: go/static-analysis
go/static-analysis: go/vet go/staticcheck go/govulncheck

## go/vet: run the 'go vet' static analyzer
.PHONY: go/vet
go/vet:
	-go vet ./...

## go/staticcheck: run the 'staticcheck' static analyzer
.PHONY: go/staticcheck
go/staticcheck:
	-$$(go env GOPATH)/bin/staticcheck ./...

## go/govulncheck: run the 'govulncheck' static analyzer
.PHONY: go/govulncheck
go/govulncheck:
	-$$(go env GOPATH)/bin/govulncheck ./...

## go/install-tools: install/upgrade the static analysis and documentation tools
# ## staticcheck, pkgsite
.PHONY: go/install-tools
go/install-tools:
	go install honnef.co/go/tools/cmd/staticcheck@latest
	go install golang.org/x/vuln/cmd/govulncheck@latest
	go install golang.org/x/pkgsite/cmd/pkgsite@latest
