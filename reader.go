package localmq

// topicReader is the message reader interface.
type topicReader interface {
	doTopic() *Topic
	doChan() chan *Message
	doClose()
}

// Reader waits for messages to be published/written to a topic, and then writes
// those messages to a channel for a subscriber to consume.
//
// Created by a Topic.
type Reader struct {
	reader topicReader
}

// Topic returns the reader's Topic.
func (r *Reader) Topic() *Topic {
	return r.reader.doTopic()
}

// Chan returns the reader message channel and can be used to read published messages.
//
// A range loop or comma ok idiom can be used to detect a closed topic (existing
// topic messages will still be delivered before the channel is closed) or a
// closed reader (no existing topic messages will be delivered).
func (r *Reader) Chan() chan *Message {
	return r.reader.doChan()
}

// Close closes the reader.
func (r *Reader) Close() {
	r.reader.doClose()
}
