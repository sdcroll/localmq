package localmq_test

import (
	"encoding/json"
	"slices"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/sdcroll/localmq"
)

func TestReaderWriter(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	topicList := broker.Topics()
	assert.Equal(1, len(topicList))
	assert.True(slices.Contains(topicList, "test topic"))

	tp, err := broker.Topic("test topic")
	require.Nil(err)
	require.NotNil(tp)
	assert.Equal(tp, topic)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()
	assert.Equal("test topic", r.Topic().Name())

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)
	assert.Equal("test topic", w.Topic().Name())

	stats := topic.Stats()
	assert.Equal("test topic", stats.Name)
	assert.Equal(false, stats.Closed)
	assert.Equal(1, stats.Readers)
	assert.Equal(0, stats.Logs)
	assert.Equal(0, stats.LogsWritten)
	assert.Equal(0, stats.LogsRead)
	assert.Equal(0, stats.LogsDropped)
}

func TestJsonBrokerStats(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic1, err := broker.NewTopic("test topic 1")
	require.Nil(err)
	require.NotNil(topic1)

	topic2, err := broker.NewTopic("test topic 2")
	require.Nil(err)
	require.NotNil(topic2)

	brokerStats := broker.Stats()

	b, err := json.MarshalIndent(brokerStats, "", "  ")
	require.Nil(err)

	s := string(b)
	assert.Contains(s, `"topics": [`)
	assert.Contains(s, `"name": "test topic 1"`)
	assert.Contains(s, `"name": "test topic 2"`)
	assert.Contains(s, `"closed": false`)
	assert.Contains(s, `"readers": 0`)
	assert.Contains(s, `"min_readers": 0`)
	assert.Contains(s, `"anycast_groups": 0`)
	assert.Contains(s, `"logs": 0`)
	assert.Contains(s, `"logs_written": 0`)
	assert.Contains(s, `"logs_read": 0`)
	assert.Contains(s, `"logs_dropped": 0`)
}

func TestBrokerNewReader(t *testing.T) {

	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r1, err := broker.NewReader("test topic")
	require.Nil(err)
	require.NotNil(r1)
	defer r1.Close()

	r2, err := broker.NewReader("test topic", localmq.WithAnycastGroup("group"))
	require.Nil(err)
	require.NotNil(r2)
	defer r2.Close()
}

func TestBrokerNewReaderTopicNotFound(t *testing.T) {

	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	r1, err := broker.NewReader("test topic")
	require.ErrorIs(err, localmq.ErrTopicNotFound)
	require.Equal(err.Error(), "topic \"test topic\" not found")
	require.Nil(r1)

	r2, err := broker.NewReader("test topic", localmq.WithAnycastGroup("group"))
	require.ErrorIs(err, localmq.ErrTopicNotFound)
	require.Equal(err.Error(), "topic \"test topic\" not found")
	require.Nil(r2)
}

func TestTopicNotFound(t *testing.T) {

	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.Topic("test topic")
	require.ErrorIs(err, localmq.ErrTopicNotFound)
	require.Equal(err.Error(), "topic \"test topic\" not found")
	require.Nil(topic)
}

func TestDuplicateTopic(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	topic, err = broker.NewTopic("test topic")
	require.Nil(topic)
	require.ErrorIs(err, localmq.ErrTopicExists)
	require.Equal(err.Error(), "topic \"test topic\" exists")

	topicList := broker.Topics()
	assert.Equal(1, len(topicList))
	assert.True(slices.Contains(topicList, "test topic"))
}

func TestDuplicateTopicClose(t *testing.T) {

	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	topic.Close()
	topic.Close()
}

func TestTopicClosedError(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r1, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r1)
	defer r1.Close()

	w1, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w1)

	topic.Close()

	err = w1.Publish("foo")
	require.ErrorIs(err, localmq.ErrTopicClosed)
	assert.Equal(err.Error(), "topic \"test topic\" closed")

	msg, ok := <-r1.Chan()
	require.Nil(msg)
	assert.False(ok)

	r2, err := topic.NewReader()
	require.ErrorIs(err, localmq.ErrTopicClosed)
	assert.Equal(err.Error(), "topic \"test topic\" closed")
	require.Nil(r2)

	w2, err := topic.NewWriter()
	require.ErrorIs(err, localmq.ErrTopicClosed)
	assert.Equal(err.Error(), "topic \"test topic\" closed")
	require.Nil(w2)
}

func TestTopicStatsFound(t *testing.T) {

	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	stats, err := broker.TopicStats("test topic")
	require.Nil(err)
	require.NotNil(stats)
}

func TestTopicStatsNotFound(t *testing.T) {

	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	stats, err := broker.TopicStats("test topic")
	require.ErrorIs(err, localmq.ErrTopicNotFound)
	require.Equal(err.Error(), "topic \"test topic\" not found")
	require.Nil(stats)
}

func TestLogsDroppedNoReaders(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	const numWrites = 5
	for i := 0; i < numWrites; i++ {
		err = w.Publish("foo")
		require.Nil(err)
	}

	stats := topic.Stats()
	assert.Equal(0, stats.LogsWritten)
	assert.Equal(numWrites, stats.LogsDropped)
}

func TestLogsDroppedOneReader(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	const numWrites = 5
	for i := 0; i < numWrites; i++ {
		err = w.Publish("foo")
		require.Nil(err)
	}

	stats := topic.Stats()
	assert.Equal(5, stats.Logs)
	assert.Equal(5, stats.LogsWritten)
	assert.Equal(0, stats.LogsRead)
	assert.Equal(0, stats.LogsDropped)

	r.Close()

	stats = topic.Stats()
	assert.Equal(0, stats.Logs)
	assert.Equal(5, stats.LogsWritten)
	assert.Equal(0, stats.LogsRead)
	assert.Equal(5, stats.LogsDropped)
}

func TestNoLogsDroppedWithMinReaders(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic", localmq.WithMinReaders(1))
	require.Nil(err)
	require.NotNil(topic)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	const numWrites = 5
	for i := 0; i < numWrites; i++ {
		err = w.Publish("foo")
		require.Nil(err)
	}

	stats := topic.Stats()
	assert.Equal(5, stats.Logs)
	assert.Equal(numWrites, stats.LogsWritten)
	assert.Equal(0, stats.LogsRead)
	assert.Equal(0, stats.LogsDropped)

	r.Close()

	stats = topic.Stats()
	assert.Equal(5, stats.Logs)
	assert.Equal(numWrites, stats.LogsWritten)
	assert.Equal(0, stats.LogsRead)
	assert.Equal(0, stats.LogsDropped)
}

func TestWriteReadFive(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	const numWrites = 5
	for i := 0; i < numWrites; i++ {
		err = w.Publish(i)
		require.Nil(err)
	}

	stats := topic.Stats()
	assert.Equal(numWrites, stats.Logs)

	for i := 0; i < numWrites; i++ {
		msg, ok := <-r.Chan()
		assert.True(ok)
		assert.Equal(i, msg.Data.(int))
	}

	topic.Quiesce()
	stats = topic.Stats()
	assert.Equal(1, stats.Readers)
	assert.Equal(0, stats.Logs)
	assert.Equal(numWrites, stats.LogsWritten)
	assert.Equal(numWrites, stats.LogsRead)
	assert.Equal(0, stats.LogsDropped)
}

func TestWithMinReaders1(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic", localmq.WithMinReaders(1))
	require.Nil(err)
	require.NotNil(topic)

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	const numWrites = 5
	for i := 0; i < numWrites; i++ {
		err = w.Publish(i)
		require.Nil(err)
	}

	stats := topic.Stats()
	assert.Equal(numWrites, stats.Logs)
	assert.Equal(0, stats.LogsDropped)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()

	for i := 0; i < numWrites; i++ {
		msg, ok := <-r.Chan()
		assert.True(ok)
		assert.Equal(i, msg.Data.(int))
	}

	topic.Quiesce()
	stats = topic.Stats()
	assert.Equal(1, stats.Readers)
	assert.Equal(0, stats.Logs)
	assert.Equal(numWrites, stats.LogsWritten)
	assert.Equal(numWrites, stats.LogsRead)
	assert.Equal(0, stats.LogsDropped)
}

func TestWithMinReaders2(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic", localmq.WithMinReaders(2))
	require.Nil(err)
	require.NotNil(topic)

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	const numWrites = 5
	for i := 0; i < numWrites; i++ {
		err = w.Publish(i)
		require.Nil(err)
	}

	stats := topic.Stats()
	assert.Equal(numWrites, stats.Logs)
	assert.Equal(0, stats.LogsDropped)

	r1, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r1)
	defer r1.Close()

	for i := 0; i < numWrites; i++ {
		msg, ok := <-r1.Chan()
		assert.True(ok)
		assert.Equal(i, msg.Data.(int))
	}

	r2, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r2)
	defer r2.Close()

	for i := 0; i < numWrites; i++ {
		msg, ok := <-r2.Chan()
		assert.True(ok)
		assert.Equal(i, msg.Data.(int))
	}

	topic.Quiesce()
	stats = topic.Stats()
	assert.Equal(2, stats.Readers)
	assert.Equal(0, stats.Logs)
	assert.Equal(numWrites, stats.LogsWritten)
	assert.Equal(2*numWrites, stats.LogsRead)
	assert.Equal(0, stats.LogsDropped)
}

func TestReadWithSelect(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	const numWrites = 5
	for i := 0; i < numWrites; i++ {
		err = w.Publish(i)
		require.Nil(err)
	}

	numReads := 0
	for numReads < numWrites {
		msg, ok := <-r.Chan()
		assert.True(ok)
		assert.Equal(numReads, msg.Data.(int))
		numReads += 1
	}
}

func TestReadWithSelectClose(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	const numWrites = 5
	for i := 0; i < numWrites; i++ {
		err = w.Publish(i)
		require.Nil(err)
	}

	topic.Close()

	numReads := 0

	for msg := range r.Chan() {
		assert.Equal(numReads, msg.Data.(int))
		numReads += 1
	}

	assert.Equal(numWrites, numReads)
}

func TestRangeRead(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	const numWrites = 5
	for i := 0; i < numWrites; i++ {
		err = w.Publish(i)
		require.Nil(err)
	}

	topic.Close()

	numReads := 0
	for msg := range r.Chan() {
		assert.Equal(numReads, msg.Data.(int))
		numReads += 1
	}

	assert.Equal(numWrites, numReads)
}

func TestMulticast(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r1, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r1)
	defer r1.Close()

	r2, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r2)
	defer r2.Close()

	r3, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r3)
	defer r3.Close()

	stats := topic.Stats()
	assert.Equal(3, stats.Readers)

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	const numWrites = 5
	for i := 0; i < numWrites; i++ {
		err = w.Publish(i)
		require.Nil(err)
	}

	topic.Close()

	for _, r := range []*localmq.Reader{r1, r2, r3} {
		numReads := 0
		for msg := range r.Chan() {
			assert.Equal(numReads, msg.Data.(int))
			numReads += 1
		}

		assert.Equal(numWrites, numReads)
	}
}

func TestCloseReader(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	const numWrites = 5
	for i := 0; i < numWrites; i++ {
		err = w.Publish(i)
		require.Nil(err)
	}

	stats := topic.Stats()
	assert.Equal(1, stats.Readers)
	assert.Equal(numWrites, stats.Logs)

	r.Close()
	_, ok := <-r.Chan()
	assert.False(ok)

	stats = topic.Stats()
	assert.Equal(0, stats.Readers)
	assert.Equal(0, stats.Logs)

	// multiple close is ok
	r.Close()

	msg, ok := <-r.Chan()
	require.Nil(msg)
	assert.False(ok)
}

func TestCloseReaders(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r0, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r0)
	defer r0.Close()

	r1, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r1)
	defer r1.Close()

	r2, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r2)
	defer r2.Close()

	r3, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r3)
	defer r3.Close()

	r4, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r4)
	defer r4.Close()

	r5, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r5)
	defer r5.Close()

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	const numWrites = 5
	for i := 0; i < numWrites; i++ {
		err = w.Publish(i)
		require.Nil(err)
	}
	topic.Close()

	r0.Close()

	for _, r := range []*localmq.Reader{r1, r2, r3, r4, r5} {
		numReads := 0
		for msg := range r.Chan() {
			assert.Equal(numReads, msg.Data.(int))
			numReads += 1

			if r == r1 && numReads == 1 {
				r1.Close()
				break
			}

			if r == r2 && numReads == 2 {
				r2.Close()
				break
			}

			if r == r3 && numReads == 3 {
				r3.Close()
				break
			}

			if r == r4 && numReads == 4 {
				r4.Close()
				break
			}
		}

		switch r {
		case r1:
			assert.Equal(1, numReads)
		case r2:
			assert.Equal(2, numReads)
		case r3:
			assert.Equal(3, numReads)
		case r4:
			assert.Equal(4, numReads)
		default:
			assert.Equal(numWrites, numReads)
		}
	}

	topic.Quiesce()
	stats := topic.Stats()
	assert.Equal(1, stats.Readers)
	assert.Equal(0, stats.Logs)
	assert.Equal(numWrites, stats.LogsWritten)
	assert.Equal(0+1+2+3+4+5, stats.LogsRead)
}
