package localmq_test

import (
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/sdcroll/localmq"
)

func TestConsumerGroupOfOne(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)

	r, err := topic.NewReader(localmq.WithAnycastGroup("group"))
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()
	assert.Equal("test topic", r.Topic().Name())

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)
	assert.Equal("test topic", w.Topic().Name())

	numWrite := 5

	for i := 0; i < numWrite; i++ {
		err = w.Publish(i)
		require.Nil(err)
	}

	for i := 0; i < numWrite; i++ {
		msg, ok := <-r.Chan()
		require.NotNil(msg)
		assert.True(ok)
		assert.Equal(i, msg.Data.(int))
	}

	select {
	case <-r.Chan():
		assert.Fail("unexpected read success")
	default:
	}

	topic.Quiesce()
	stats := topic.Stats()
	require.NotNil(stats)
	assert.Equal("test topic", stats.Name)
	assert.Equal(false, stats.Closed)
	assert.Equal(1, stats.Readers)
	assert.Equal(1, stats.AnycastGroups)
	assert.Equal(0, stats.Logs)
	assert.Equal(numWrite, stats.LogsWritten)
	assert.Equal(numWrite, stats.LogsRead)
	assert.Equal(0, stats.LogsDropped)

	r.Close()
	msg, ok := <-r.Chan()
	assert.False(ok)
	require.Nil(msg)

	topic.Quiesce()
	stats = topic.Stats()
	assert.Equal(0, stats.Readers)
	assert.Equal(0, stats.AnycastGroups)
}

func TestAnycastGroup(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	readMessages := func(id int, r *localmq.Reader, results chan<- int) {

		defer r.Close()

		reads := 0
		last := 0
		for msg := range r.Chan() {
			curr := msg.Data.(int)
			reads += 1
			assert.Greater(curr, last)
			last = curr

			time.Sleep(1 * time.Millisecond)
		}

		results <- reads
	}

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic", localmq.WithMinReaders(1))
	require.Nil(err)
	require.NotNil(topic)

	var wg sync.WaitGroup

	numReaders := 10
	results := make(chan int)

	for i := 0; i < numReaders; i++ {
		r, err := topic.NewReader(localmq.WithAnycastGroup("group"))
		require.Nil(err)
		require.NotNil(r)

		wg.Add(1)
		go func(id int, reader *localmq.Reader, results chan<- int) {
			defer wg.Done()
			readMessages(id, reader, results)
		}(i, r, results)
	}

	stats := topic.Stats()
	require.NotNil(stats)
	assert.Equal("test topic", stats.Name)
	assert.Equal(false, stats.Closed)
	assert.Equal(1, stats.Readers)
	assert.Equal(1, stats.AnycastGroups)

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	numWrite := 1000

	for i := 0; i < numWrite; i++ {
		err = w.Publish(i + 1)
		require.Nil(err)
	}

	topic.Close()

	totalReadCount := 0
	for i := 0; i < numReaders; i++ {
		readCount := <-results
		assert.Greater(readCount, 75) // probably conservative (?)
		totalReadCount += readCount
	}
	assert.Equal(totalReadCount, numWrite)

	wg.Wait()

	topic.Quiesce()
	stats = topic.Stats()
	require.NotNil(stats)
	assert.Equal("test topic", stats.Name)
	assert.Equal(true, stats.Closed)
	assert.Equal(0, stats.Readers)
	assert.Equal(0, stats.AnycastGroups)
	assert.Equal(0, stats.Logs)
	assert.Equal(numWrite, stats.LogsWritten)
	assert.Equal(numWrite, stats.LogsRead)
	assert.Equal(0, stats.LogsDropped)
}
