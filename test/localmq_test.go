package localmq_test

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/sdcroll/localmq"
)

func TestFiveSameGoroutine(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()
	assert.Equal("test topic", r.Topic().Name())

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)
	assert.Equal("test topic", w.Topic().Name())

	numWrite := 5

	for i := 0; i < numWrite; i++ {
		err = w.Publish(i)
		require.Nil(err)
	}

	for i := 0; i < numWrite; i++ {
		msg, ok := <-r.Chan()
		require.NotNil(msg)
		assert.True(ok)
		assert.Equal(i, msg.Data.(int))
	}

	select {
	case <-r.Chan():
		assert.Fail("unexpected read success")
	default:
	}

	topic.Quiesce()
	stats := topic.Stats()
	require.NotNil(stats)
	assert.Equal("test topic", stats.Name)
	assert.Equal(false, stats.Closed)
	assert.Equal(1, stats.Readers)
	assert.Equal(0, stats.Logs)
	assert.Equal(numWrite, stats.LogsWritten)
	assert.Equal(numWrite, stats.LogsRead)
	assert.Equal(0, stats.LogsDropped)

	r.Close()
	_, ok := <-r.Chan()
	assert.False(ok)

	stats = topic.Stats()
	assert.Equal(0, stats.Readers)
}

func TestFiveSameGoroutineWithClose(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()
	assert.Equal("test topic", r.Topic().Name())

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)
	assert.Equal("test topic", w.Topic().Name())

	numWrite := 5

	for i := 0; i < numWrite; i++ {
		err = w.Publish(i)
		require.Nil(err)
	}

	topic.Close()

	numRead := 0
	for msg := range r.Chan() {
		require.NotNil(msg)
		numRead += 1
	}

	assert.Equal(numRead, numWrite)

	stats := topic.Stats()
	require.NotNil(stats)
	assert.Equal("test topic", stats.Name)
	assert.Equal(true, stats.Closed)
	assert.Equal(1, stats.Readers)
	assert.Equal(0, stats.Logs)
	assert.Equal(numWrite, stats.LogsWritten)
	assert.Equal(numWrite, stats.LogsRead)
	assert.Equal(0, stats.LogsDropped)

	r.Close()
	_, ok := <-r.Chan()
	assert.False(ok)

	stats = topic.Stats()
	assert.Equal(0, stats.Readers)
}

func TestPingPong(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	play := func(topic *localmq.Topic, r *localmq.Reader, maxRead int) {

		w, err := topic.NewWriter()
		require.Nil(err)
		require.NotNil(w)

		msgId := 0

		if r.Topic().Name() == "ping" {
			err = w.Publish(msgId)
			require.Nil(err)
			msgId += 1
		}

		numRead := 0

		for msg := range r.Chan() {
			assert.Equal(numRead, msg.Data.(int))
			numRead += 1

			err = w.Publish(msgId)
			require.Nil(err)
			msgId += 1

			if numRead >= maxRead {
				return
			}
		}
	}

	broker := localmq.NewBroker()
	require.NotNil(broker)

	pingTopic, err := broker.NewTopic("ping")
	require.Nil(err)
	require.NotNil(pingTopic)

	pongTopic, err := broker.NewTopic("pong")
	require.Nil(err)
	require.NotNil(pongTopic)

	pingReader, err := pingTopic.NewReader()
	require.Nil(err)
	require.NotNil(pingReader)

	pongReader, err := pongTopic.NewReader()
	require.Nil(err)
	require.NotNil(pongReader)

	var wg sync.WaitGroup

	maxRead := 100

	wg.Add(1)
	go func() {
		defer wg.Done()
		play(pingTopic, pongReader, maxRead)
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		play(pongTopic, pingReader, maxRead)
	}()

	wg.Wait()

	pingReader.Close()
	pongReader.Close()

	for _, topicName := range []string{"ping", "pong"} {
		stats, err := broker.TopicStats(topicName)
		require.Nil(err)
		require.NotNil(stats)
		assert.Equal(topicName, stats.Name)
		assert.Equal(false, stats.Closed)
		assert.Equal(0, stats.Readers)
		assert.Equal(0, stats.Logs)
		assert.Equal(maxRead, stats.LogsRead)

		// ping writes one extra log, which is not read and is dropped
		if topicName == "pong" {
			assert.Equal(maxRead+1, stats.LogsWritten)
			assert.Equal(1, stats.LogsDropped)
		} else {
			assert.Equal(maxRead, stats.LogsWritten)
			assert.Equal(0, stats.LogsDropped)
		}
	}
}
