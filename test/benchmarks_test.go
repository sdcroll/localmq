package localmq_test

import (
	"fmt"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/sdcroll/localmq"
)

func BenchmarkConsumerGroup(b *testing.B) {

	var table = []struct {
		readers int
	}{
		{readers: 1},
		{readers: 2},
		{readers: 4},
		{readers: 6},
		{readers: 8},
		{readers: 12},
		{readers: 16},
	}

	numWrite := 100000

	for _, v := range table {
		b.Run(fmt.Sprintf("readers=%v", v.readers), func(b *testing.B) {

			for i := 0; i < b.N; i++ {
				consumerGroup(b, v.readers, numWrite)
			}
		})
	}
}

func consumerGroup(b *testing.B, numReaders int, numWrite int) {

	readMessages := func(id int, r *localmq.Reader, results chan<- int) {

		defer r.Close()

		reads := 0
		for msg := range r.Chan() {
			_ = msg.Data.(int)
			reads += 1
		}

		results <- reads
	}

	broker := localmq.NewBroker()

	topic, _ := broker.NewTopic("test topic", localmq.WithMinReaders(1))

	var wg sync.WaitGroup

	results := make(chan int)

	for i := 0; i < numReaders; i++ {
		r, _ := topic.NewReader(localmq.WithAnycastGroup("group"))

		wg.Add(1)
		go func(id int, reader *localmq.Reader, results chan<- int) {
			defer wg.Done()
			readMessages(id, reader, results)
		}(i, r, results)
	}

	w, _ := topic.NewWriter()

	for i := 0; i < numWrite; i++ {
		w.Publish(i + 1)
	}

	topic.Close()

	totalReadCount := 0
	for i := 0; i < numReaders; i++ {
		readCount := <-results
		totalReadCount += readCount
	}

	assert.Equal(b, totalReadCount, numWrite)

	wg.Wait()
}

func BenchmarkMultipleReaders(b *testing.B) {

	var table = []struct {
		readers int
	}{
		{readers: 1},
		{readers: 2},
		{readers: 4},
		{readers: 6},
		{readers: 8},
		{readers: 12},
		{readers: 16},
	}

	numWrite := 100000

	for _, v := range table {
		b.Run(fmt.Sprintf("readers=%v", v.readers), func(b *testing.B) {

			for i := 0; i < b.N; i++ {
				multipleReaders(b, v.readers, numWrite)
			}
		})
	}
}

func multipleReaders(b *testing.B, numReaders int, numWrite int) {

	readMessages := func(id int, r *localmq.Reader, results chan<- int) {

		defer r.Close()

		reads := 0
		for msg := range r.Chan() {
			_ = msg.Data.(int)
			reads += 1
		}

		results <- reads
	}

	broker := localmq.NewBroker()

	topic, _ := broker.NewTopic("test topic", localmq.WithMinReaders(1))

	var wg sync.WaitGroup

	results := make(chan int)

	for i := 0; i < numReaders; i++ {
		r, _ := topic.NewReader()

		wg.Add(1)
		go func(id int, reader *localmq.Reader, results chan<- int) {
			defer wg.Done()
			readMessages(id, reader, results)
		}(i, r, results)
	}

	w, _ := topic.NewWriter()

	for i := 0; i < numWrite; i++ {
		w.Publish(i + 1)
	}

	topic.Close()

	totalReadCount := 0
	for i := 0; i < numReaders; i++ {
		readCount := <-results
		totalReadCount += readCount
	}

	assert.Equal(b, totalReadCount, numReaders*numWrite)

	wg.Wait()
}
