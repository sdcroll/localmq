package localmq_test

import (
	"context"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/sdcroll/localmq"
)

func TestRequestSuccess(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()

		n := 42
		resp, err := w.Request(n)
		require.Nil(err)
		assert.Equal(2*n, resp.(int))
	}()

	msg, ok := <-r.Chan()
	assert.True(ok)

	require.NotNil(msg.Reply)
	err = msg.Reply(2 * msg.Data.(int))
	require.Nil(err)

	wg.Wait()
}

func TestRequestMultipleReplies(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()

		n := 42
		resp, err := w.Request(n)
		require.Nil(err)
		assert.Equal(2*n, resp.(int))
	}()

	msg, ok := <-r.Chan()
	assert.True(ok)

	require.NotNil(msg.Reply)

	for i := 0; i < 3; i++ {
		err = msg.Reply(2 * msg.Data.(int))
		require.Nil(err)
	}

	wg.Wait()
}

func TestRequestGroupSuccess(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r, err := topic.NewReader(localmq.WithAnycastGroup("group"))
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()

		n := 42
		resp, err := w.Request(n)
		require.Nil(err)
		assert.Equal(2*n, resp.(int))
	}()

	msg, ok := <-r.Chan()
	assert.True(ok)

	require.NotNil(msg.Reply)

	err = msg.Reply(2 * msg.Data.(int))
	require.Nil(err)

	wg.Wait()
}

func TestWriteMessageReadRequest(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()

		n := 42
		err := w.Publish(n)
		require.Nil(err)
	}()

	msg, ok := <-r.Chan()
	assert.True(ok)

	require.Nil(msg.Reply)
	assert.Equal(42, msg.Data.(int))

	wg.Wait()
}

func TestRequestTopicClosed(t *testing.T) {

	require := require.New(t)
	assert := assert.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	topic.Close()

	resp, err := w.Request(42)
	require.ErrorIs(err, localmq.ErrTopicClosed)
	assert.Equal(err.Error(), "topic \"test topic\" closed")
	require.Nil(resp)
}

func TestRequestWithBackgroundContext(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()

		n := 42
		resp, err := w.Request(n, localmq.WithContext(context.Background()))
		require.Nil(err)
		assert.Equal(2*n, resp.(int))
	}()

	msg, ok := <-r.Chan()
	assert.True(ok)

	require.NotNil(msg.Reply)
	err = msg.Reply(2 * msg.Data.(int))
	require.Nil(err)

	wg.Wait()
}

func TestRequestWithTimoutContext(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	broker := localmq.NewBroker()
	require.NotNil(broker)

	topic, err := broker.NewTopic("test topic")
	require.Nil(err)
	require.NotNil(topic)

	r, err := topic.NewReader()
	require.Nil(err)
	require.NotNil(r)
	defer r.Close()

	w, err := topic.NewWriter()
	require.Nil(err)
	require.NotNil(w)

	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()

		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Millisecond)
		defer cancel()

		n := 42
		resp, err := w.Request(n, localmq.WithContext(ctx))
		require.Nil(resp)
		require.NotNil(err)
	}()

	msg, ok := <-r.Chan()
	assert.True(ok)

	require.NotNil(msg)
	require.NotNil(msg.Reply)

	wg.Wait()
}
