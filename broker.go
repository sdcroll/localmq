package localmq

import (
	"sync"
)

// Broker is a local in-process message broker.
//
// A Broker provides topic-based messaging and uses a publish-subscribe
// model. Multicast and anycast message delivery is supported.
type Broker struct {
	mu     sync.Mutex
	topics map[string]*Topic // key: topic name
	notify chan int
	closed bool
}

// NewBroker constructs a new Broker.
func NewBroker() *Broker {

	return &Broker{
		topics: make(map[string]*Topic),
		notify: make(chan int),
		closed: false,
	}
}

// topicConfig holds the minimum number of readers value for the WithMinReaders
// option.
type topicConfig struct {
	minReaders uint
}

// BrokerNewTopicOption is used by the WithMinReaders option.
type BrokerNewTopicOption func(*topicConfig)

// WithMinReaders option configures the Topic's minimum readers.
//
// If the topic's minimum readers value is greater than zero, messages for the
// topic will be retained until the minimum number of readers have registered
// for the topic. Any new readers created for a topic, above the min readers
// threshold, will only read new logs.
//
// This is intended to be used in lieu of a time-based or message count
// retention mechanism.
func WithMinReaders(minReaders uint) BrokerNewTopicOption {
	return func(tc *topicConfig) {
		tc.minReaders = minReaders
	}
}

// NewTopic constructs a new Topic.
//
// An error will be returned if the topic cannot be created.
func (b *Broker) NewTopic(topicName string, options ...BrokerNewTopicOption) (*Topic, error) {

	b.mu.Lock()
	defer b.mu.Unlock()

	_, ok := b.topics[topicName]

	if ok {
		return nil, &topicExists{topicName}
	}

	config := &topicConfig{}

	for _, option := range options {
		option(config)
	}

	topic := newTopic(topicName, b, config.minReaders)
	b.topics[topicName] = topic

	return topic, nil
}

// NewReader constructs a new Reader and subscribes to the specified topic.
//
// An error will be returned if the topic does not exist or if the reader cannot
// be created.
//
// Note that this is a convenience method. See the Topic.NewReader method for
// parameter details.
func (b *Broker) NewReader(topicName string, options ...TopicNewReaderOption) (*Reader, error) {

	t, err := b.Topic(topicName)
	if t == nil {
		return nil, err
	}

	r, err := t.NewReader(options...)
	return r, err
}

// Topic returns the specified Topic if it exists, or an error otherwise.
func (b *Broker) Topic(topicName string) (*Topic, error) {

	b.mu.Lock()
	defer b.mu.Unlock()

	var ok bool
	topic, ok := b.topics[topicName]

	if !ok {
		return nil, &topicNotFound{topicName}
	}

	return topic, nil
}

// Topics returns a slice of topic names.
func (b *Broker) Topics() []string {

	b.mu.Lock()
	defer b.mu.Unlock()

	s := make([]string, 0, len(b.topics))

	for _, topic := range b.topics {
		s = append(s, topic.name)
	}

	return s
}

// Stats holds the stats for each topic managed by the broker.
type BrokerStats struct {
	Topics []*TopicStats `json:"topics"`
}

// Stats returns the stats for each topic managed by the broker.
func (b *Broker) Stats() *BrokerStats {

	b.mu.Lock()
	defer b.mu.Unlock()

	s := &BrokerStats{Topics: make([]*TopicStats, 0, len(b.topics))}

	for _, topic := range b.topics {
		s.Topics = append(s.Topics, topic.Stats())
	}

	return s
}

// TopicStats returns the stats for the specified topic.
//
// An error will be returned if the topic does not exist.
//
// See also the Topic.Stats method.
func (b *Broker) TopicStats(topicName string) (*TopicStats, error) {

	t, err := b.Topic(topicName)
	if t == nil {
		return nil, err
	}

	return t.Stats(), nil
}

// deleteTopic deletes the topic from the broker. Called by Topic.Close().
func (b *Broker) deleteTopic(topic *Topic) {

	topicName := topic.Name()

	b.mu.Lock()
	defer b.mu.Unlock()

	delete(b.topics, topicName)
}
