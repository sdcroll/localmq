package localmq

import (
	"context"
)

// Writer publishes/writes messages to a topic.
type Writer struct {
	topic *Topic // parent topic
}

// newWriter constructs a new Writer.
//
// Called by Topic.
func newWriter(topic *Topic) *Writer {
	return &Writer{topic: topic}
}

// Topic returns the writer's topic.
func (w *Writer) Topic() *Topic {
	return w.topic
}

// Publish a message to the topic.
//
// An error will be returned is the message cannot be published.
func (w *Writer) Publish(data interface{}) error {

	return w.topic.writeMessage(data, nil)
}

// writerRequestConfig holds the context for the WithContext option.
type writerRequestConfig struct {
	ctx context.Context
}

// WriterRequestOption is used by the WithContext option.
type WriterRequestOption func(*writerRequestConfig)

// WithContext option configures the Writer's context when making a Request.
func WithContext(ctx context.Context) WriterRequestOption {
	return func(rc *writerRequestConfig) {
		rc.ctx = ctx
	}
}

// Request publishes a message to the topic and then awaits and returns a reply.
//
// An optional context can be specified to provide a deadline or cancellation
// signal.
//
// An error will be returned if the message cannot be published or if the
// context was canceled before a reply was received.
//
// Note that message publication and delivery will NOT be affected by context
// cancellation, only the wait for the reply will be interrupted by the
// cancellation.
func (w *Writer) Request(data interface{}, options ...WriterRequestOption) (interface{}, error) {

	config := &writerRequestConfig{}

	for _, option := range options {
		option(config)
	}

	replyCh := make(chan interface{}, 1)

	reply := func(replyMsg interface{}) error {

		select {
		case replyCh <- replyMsg:
		default:
		}

		return nil
	}

	if err := w.topic.writeMessage(data, reply); err != nil {
		return nil, err
	}

	if config.ctx == nil {
		return <-replyCh, nil
	}

	select {
	case replyMsg := <-replyCh:
		return replyMsg, nil
	case <-config.ctx.Done():
		return nil, config.ctx.Err()
	}
}
