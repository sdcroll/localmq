package localmq

import (
	"sync"
)

// multicastReader waits for messages to be published/written to a topic, and
// then writes those messages to a channel for a subscriber to consume.
type multicastReader struct {
	topic        *Topic          // parent topic
	readPos      *topicLog       // log read position
	readerClosed bool            // if true, indicates the reader has been closed
	reading      bool            // if false, indicates the topic has been closed
	ch           chan *Message   // used to deliver messages to the client/subscriber
	anycastGroup string          // name of the anycast group (TODO not currently used)
	refCount     int             // postive if the reader is a backend for an anycast group
	interrupt    chan struct{}   // used to interrupt an attempted write to the message channel
	chanWriter   sync.WaitGroup  // used to wait for the runChanWriter goroutine to exit
	quiescing    *sync.WaitGroup // used to indicate the reader has quiesced
}

// newMulticastReader constructs a new multicastReader.
//
// Called by Topic.
func newMulticastReader(topic *Topic, readPos *topicLog, anycastGroup string) *multicastReader {

	reader := &multicastReader{
		topic:        topic,
		readPos:      readPos,
		reading:      true,
		ch:           make(chan *Message),
		anycastGroup: anycastGroup,
		interrupt:    make(chan struct{}),
	}

	reader.chanWriter.Add(1)
	go reader.runChanWriter()

	return reader
}

// runChanWriter is a goroutine that waits for messages to be published to a
// topic and then delivers those messages to the subscriber via the reader's
// message channel.
//
// In general operation, the goroutine is either waiting on the topic's
// condition variable or is attempting/waiting to deliver a message to the
// reader's message channel.
//
// The condition variable is used to indicate that a message has been published,
// either the reader or the topic has been closed, or that the topic's Quiesce()
// method has been called.
//
// If the goroutine is blocked waiting to deliver a message, it can be
// interrupted via the interrupt channel, which is utilized when the reader is
// closed or quiescing.
func (r *multicastReader) runChanWriter() {

	defer r.chanWriter.Done()

	cond := r.topic.cond
	cond.L.Lock()
	defer cond.L.Unlock()

	logsRead := 0

	for {
		if logsRead > 0 {
			r.readPos = r.topic.consumeLog(r.readPos)
			logsRead = 0
		}

		if r.readerClosed {
			r.topic.deleteReader(r)
			if r.reading {
				close(r.ch)
			}

			return
		}

		if r.quiescing != nil {
			r.quiescing.Done()
			r.quiescing = nil
		}

		if !r.reading {
			// topic was closed, wait for reader to close
			cond.Wait()
			continue
		}

		if msg := r.topic.getMessage(r.readPos); msg != nil {

			cond.L.Unlock()
			{
				// try to write message to the consumer
				select {
				case r.ch <- msg:
					logsRead += 1
				case <-r.interrupt:
				}

			}
			cond.L.Lock()

			continue
		}

		// if we are here and the topic is closed, there will be no
		// more new messages available
		if r.topic.closed {
			// close the channel to ensure that any subsequent channel
			// read by the client will not block, otherwise we could
			// deadlock if Close() has not yet been called
			close(r.ch)
			r.reading = false
			continue
		}

		// wait for new messages
		cond.Wait()
	}
}

// doTopic implements a Reader interface method.
func (r *multicastReader) doTopic() *Topic {
	return r.topic
}

// doChan implemnts a Reader interface method.
func (r *multicastReader) doChan() chan *Message {
	return r.ch
}

// doClose implements a Reader interface method.
//
// Closes the reader and waits for the runChanWriter goroutine to exit. Note
// that waiting for goroutine exit ensures that no messages will be
// (asynchronously) delivered after the reader has been closed and before the
// goroutine has detected the close indication.
func (r *multicastReader) doClose() {

	if r.close() {
		r.chanWriter.Wait()
	}
}

// close conditionally closes the reader.
//
// If the reader is NOT a backend for an anycast group, the reader will be closed.
//
// If the reader IS the backend for an anycast group, the reader will be closed
// when the last member of the anycast group is closed.
func (r *multicastReader) close() bool {

	r.topic.mu.Lock()
	defer r.topic.mu.Unlock()

	if r.readerClosed {
		return true
	}

	// can go negative if not representing an anycast group
	r.refCount -= 1

	if r.refCount > 0 {
		return false
	}

	r.readerClosed = true

	close(r.interrupt)

	// Note that while it's inefficient to broadcast to all topic
	// readers when only one reader is closed, using a single
	// topic-wide condition variable allows writes to be a bit more
	// cpu efficient (which is the more common case,
	// presumably). Otherwise we'd need to hold the topic lock while
	// signaling each reader individually in a loop.
	r.topic.cond.Broadcast()

	return true
}

// quiesce will cause the reader's goroutine to enter a quiescent state to
// insure subsequent Topic.Stats() calls will be accurate.
//
// Intended to be used only in test code.
//
// Prerequisite: The topic is locked.
func (r *multicastReader) quiesce(wg *sync.WaitGroup) {

	r.quiescing = wg

	select {
	case r.interrupt <- struct{}{}:
	default:
	}
}
