// Package localmq provides an in-process local message bus.
//
// A Broker provides topic-based messaging and uses a publish-subscribe
// model. Multicast and anycast message delivery is supported.
//
// A Topic holds the logs for the specified topic. One or more writers
// publish/write logs to a topic, and one or more readers can read those logs.
//
// A Reader waits for messages to be published/written to a topic, and then writes
// those messages to a channel for a subscriber to consume.
//
// There are two types of readers: anycast and multicast.
//
// An anycast reader will be a member of an anycast group and a log will be
// delivered to one of the members of the group.
//
// In contrast, every log will be delivered to multicast reader of the topic.
//
// A Writer publishes/writes messages to a topic. Writers can also publish a
// request and await a reply. A context can be specified to provide a deadline
// or cancellation signal.
//
// Note that localmq is an ephemeral message bus that does not provide support
// for any sort of durability (disk storage).
package localmq
