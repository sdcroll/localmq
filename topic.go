package localmq

import (
	"errors"
	"fmt"
	"sync"
)

var (
	// ErrTopicExists indicates that a topic with the same name has already been
	// created.
	ErrTopicExists = errors.New("topic exists")

	// ErrTopicNotFound indicates that a topic has not been created or has been
	// deleted.
	ErrTopicNotFound = errors.New("topic not found")

	// ErrTopicClosed indicates that a topic has been closed and cannot be
	// written to by existing writers nor written or read by new readers or
	// writers.
	ErrTopicClosed = errors.New("topic closed")
)

// topicExists implements the Error and Unrwap interface.
type topicExists struct {
	topicName string
}

// Error returns a formatted error string containing the topic name.
func (e *topicExists) Error() string {
	return fmt.Sprintf("topic %q exists", e.topicName)
}

// Unrap returns the underlying ErrTopicExists error.
func (e *topicExists) Unwrap() error {
	return ErrTopicExists
}

// topicNotFound implements the Error and Unrwap interface.
type topicNotFound struct {
	topicName string
}

// Error returns a formatted error string containing the topic name.
func (e *topicNotFound) Error() string {
	return fmt.Sprintf("topic %q not found", e.topicName)
}

// Unrap returns the underlying ErrTopicNotFound error.
func (e *topicNotFound) Unwrap() error {
	return ErrTopicNotFound
}

// topicClosed implements the Error and Unrwap interface.
type topicClosed struct {
	topicName string
}

// Error returns a formatted error string containing the topic name.
func (e *topicClosed) Error() string {
	return fmt.Sprintf("topic %q closed", e.topicName)
}

// Unrap returns the underlying ErrTopicClosed error.
func (e *topicClosed) Unwrap() error {
	return ErrTopicClosed
}

// exists is the value used for set operations
var exists = struct{}{}

// Topic holds the logs for the specified topic. One or more writers
// publish/write logs to a topic, and one or more readers can read those logs.
//
// There are two types of readers: anycast and multicast.
//
// An anycast reader will be a member of an anycast group and a log will be
// delivered to one of the members of the group.
//
// In contrast, every log will be delivered to multicast reader of the topic.
//
// Topic maintains various statistics for informational and diagnostic purposes.
//
// Note that new Topics are created by a Broker.
type Topic struct {
	name   string     // const, topic name
	broker *Broker    // const, parent broker
	mu     sync.Mutex // protects subsequent fields
	closed bool       // if true, the topic has been closed
	cond   *sync.Cond // informs readers a new message has been published or the topic closed

	readers       map[*multicastReader]struct{} // all multicastReaders
	anycastGroups map[string]*multicastReader   // anycast group backends

	minReaders  int       // minumum number of readers to consume a message
	firstLog    *topicLog // first log or the sentinel if there are no logs
	lastLog     *topicLog // the last log is a sentinel and does not hold a message
	logCount    int       // current number of logs enqueued, not including the sentinel
	logsWritten int       // count of logs written to the topic
	logsRead    int       // count of the logs read from the topic
	logsDropped int       // count of the logs dropped, when no readers or topic closed
}

// newTopic constructs a new Topic.
func newTopic(topicName string, broker *Broker, minReaders uint) *Topic {

	// The last log is used as a sentinel, and the reference count will
	// indicate the number of readers that have read all available messages.
	//
	// When a new message is written, the sentinel's msg field will be
	// updated, and a new empty sentinel with a zero reference count will be
	// appended.
	sentinel := &topicLog{}

	topic := &Topic{
		name:          topicName,
		broker:        broker,
		readers:       make(map[*multicastReader]struct{}, 0),
		anycastGroups: make(map[string]*multicastReader, 0),
		minReaders:    int(minReaders),
		firstLog:      sentinel,
		lastLog:       sentinel,
	}
	topic.cond = sync.NewCond(&topic.mu)

	return topic
}

// Message holds the log payload and optional reply function.
type Message struct {
	Data  interface{}
	Reply func(interface{}) error
}

// topicLog holds the message and reference count.
type topicLog struct {
	next     *topicLog
	refCount int
	msg      Message
}

// TopicStats holds various topic-related statistics.
type TopicStats struct {
	Name          string `json:"name"`
	Closed        bool   `json:"closed"`
	Readers       int    `json:"readers"`
	MinReaders    int    `json:"min_readers"`
	AnycastGroups int    `json:"anycast_groups"`
	Logs          int    `json:"logs"`
	LogsWritten   int    `json:"logs_written"`
	LogsRead      int    `json:"logs_read"`
	LogsDropped   int    `json:"logs_dropped"`
}

// Name returns the name of a Topic.
func (t *Topic) Name() string {

	// name is not mutated, no lock required
	return t.name
}

// Stats returns the stats for a topic.
func (t *Topic) Stats() *TopicStats {

	t.mu.Lock()
	defer t.mu.Unlock()

	return &TopicStats{
		Name:          t.name,
		Closed:        t.closed,
		Readers:       len(t.readers),
		MinReaders:    t.minReaders,
		AnycastGroups: len(t.anycastGroups),
		Logs:          t.logCount,
		LogsWritten:   t.logsWritten,
		LogsRead:      t.logsRead,
		LogsDropped:   t.logsDropped,
	}
}

// NewWriter constructs a new Writer.
//
// An error will be returned if the writer cannot be created.
func (t *Topic) NewWriter() (*Writer, error) {

	t.mu.Lock()
	defer t.mu.Unlock()

	if t.closed {
		return nil, &topicClosed{t.name}
	}

	return newWriter(t), nil
}

// writeMessage writes a message to the topic log and notifies readers that a
// new message is available.
//
// An error will be returned if the message cannot be published.
func (t *Topic) writeMessage(data interface{}, reply func(interface{}) error) error {

	// we do not use a defer on the mutex as we want to unlock the mutex before
	// when we broadcast to the readers
	t.mu.Lock()
	{
		if t.closed {
			t.logsDropped += 1
			t.mu.Unlock()
			return &topicClosed{t.name}
		}

		if len(t.readers) == 0 && t.minReaders == 0 {
			t.logsDropped += 1
			t.mu.Unlock()
			return nil
		}

		// populate the sentinel with the data and reply function, retaining the
		// reference count
		t.lastLog.msg.Data = data
		t.lastLog.msg.Reply = reply

		// append a new empty sentinel with a zero reference count
		newSentinel := &topicLog{}
		t.lastLog.next = newSentinel
		t.lastLog = newSentinel

		t.logCount += 1
		t.logsWritten += 1
	}
	t.mu.Unlock()

	// notify the readers that a new message has been written
	t.cond.Broadcast()

	return nil
}

// readerConfig holds the anycast group name for the WithAnycastGroup option.
type readerConfig struct {
	anycastGroup string
}

// TopicNewReaderOption is used by the WithAnycastGroup option.
type TopicNewReaderOption func(*readerConfig)

// WithAnycastGroup option configures the Reader to be a member of the specified
// anycast group.
func WithAnycastGroup(anycastGroup string) TopicNewReaderOption {
	return func(rc *readerConfig) {
		rc.anycastGroup = anycastGroup
	}
}

// NewReader constructs a new Reader and subscribes to the topic.
//
// The WithAnycastGroup option can be used to configure the reader to be a
// member of an anycast group.
//
// Note that unless the number of readers is under the topic minimum readers
// threshold, new readers will only receive new logs, and will not get any
// pre-existing logs that have not yet been read by other readers.
//
// An error will be returned if the reader cannot be created.
func (t *Topic) NewReader(options ...TopicNewReaderOption) (*Reader, error) {

	config := &readerConfig{}

	for _, option := range options {
		option(config)
	}

	t.mu.Lock()
	defer t.mu.Unlock()

	if t.closed {
		return nil, &topicClosed{t.name}
	}

	// check to see if we have an existing anycast group (note that a map lookup
	// on a non-configured (empty string) anycast group will fail)
	reader, ok := t.anycastGroups[config.anycastGroup]
	if ok {
		return &Reader{reader: newAnycastReader(reader)}, nil
	}

	// If the topic does not yet have the configured minimum number of readers
	// (not including this reader), then set the read position to the first log
	// of the topic. Otherwise, set the read position to the sentinel, as new
	// readers above the minReaders threshold only read new logs.
	var readPos *topicLog = nil

	if len(t.readers) < t.minReaders {
		readPos = t.firstLog
	} else {
		readPos = t.lastLog
	}

	readPos.refCount += 1

	reader = newMulticastReader(t, readPos, config.anycastGroup)
	t.readers[reader] = exists

	if config.anycastGroup == "" {
		return &Reader{reader: reader}, nil
	}

	t.anycastGroups[config.anycastGroup] = reader

	return &Reader{reader: newAnycastReader(reader)}, nil
}

// getMessage returns the message at the specified read position.
//
// Prerequisite: The topic is locked.
func (t *Topic) getMessage(readPos *topicLog) *Message {

	// return if no messages available
	if readPos == t.lastLog {
		return nil
	}

	log := readPos
	return &log.msg
}

// consumeLog consumes a log for a reader and returns the next read position.
//
// If all readers have consumed the log, and the minimum number of readers is
// met, the log will be removed from the topic.
//
// Prerequisite: The topic is locked.
func (t *Topic) consumeLog(readPos *topicLog) *topicLog {

	// decrement the reference count on the current log
	log := readPos
	log.refCount -= 1

	// and increment the reference count on the next log (which may be the
	// sentinel if there are no more logs to be read)
	readPos = log.next
	readPos.refCount += 1

	// if there are no more references to the first log and the topic has the
	// configured minimum number of readers, remove the log as this reader held
	// the last reference
	if t.firstLog.refCount == 0 && len(t.readers) >= t.minReaders {
		t.firstLog = t.firstLog.next
		t.logCount -= 1
	}

	t.logsRead += 1

	return readPos
}

// deleteReader removes a multicast reader from the topic.
//
// Any unread logs by the reader that have already been read by other readers
// will be removed as long as the minimum readers requirement is satisfied.
//
// Prerequisite: The topic is locked.
func (t *Topic) deleteReader(r *multicastReader) {

	delete(t.readers, r)

	if r.anycastGroup != "" {
		delete(t.anycastGroups, r.anycastGroup)
	}

	// decrement the reference count on the current log (or sentinel)
	r.readPos.refCount -= 1

	// don't remove any logs if we are below the minimum number of readers
	if len(t.readers) < t.minReaders {
		return
	}

	// remove the logs we didn't read, but only those everyone else has already
	// read
	for t.firstLog != t.lastLog {
		if t.firstLog.refCount != 0 {
			break
		}

		t.firstLog = t.firstLog.next
		t.logCount -= 1

		if len(t.readers) == 0 {
			t.logsDropped += 1
		}
	}
}

// Close closes the topic, notifies existing readers, and deletes the topic from
// the broker.
//
// Existing Readers will continue to deliver any existing queued/already
// published messages to the subscriber. The subscriber can continue to read
// until the end of channel.
//
// Writers that attempt to publish a message to the topic will receive an
// ErrTopicClosed error.
//
// Once a topic has been closed, no new readers or writers can be created on the
// topic.
func (t *Topic) Close() {

	t.mu.Lock()
	defer t.mu.Unlock()

	if t.closed {
		return
	}

	t.closed = true
	t.cond.Broadcast()

	t.broker.deleteTopic(t)
}

// Quiesce waits for all readers' goroutines to enter a quiescent state to
// insure subsequent Stats() calls will be accurate.
//
// Intended to be used only in test code.
func (t *Topic) Quiesce() {

	var wg sync.WaitGroup

	t.mu.Lock()
	{
		wg.Add(len(t.readers))

		for r := range t.readers {
			r.quiesce(&wg)
		}
	}
	t.mu.Unlock()

	t.cond.Broadcast()
	wg.Wait()
}
