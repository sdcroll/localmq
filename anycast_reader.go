package localmq

// anycastReader is a frontend for a multicastReader.
//
// There can be multiple anycastReader frontends for a multicastReader backend.
type anycastReader struct {
	reader       *multicastReader
	readerClosed bool
}

var (
	// closedChannel is a closed channel and is used by all closed anycastReaders
	closedChannel chan *Message
)

// init initializes the closedChannel
func init() {
	closedChannel = make(chan *Message)
	close(closedChannel)
}

// newAnycastReader constructs a new anycastReader.
//
// The specified multicastReader's reference count is incremented.
//
// Prerequisite: The topic is locked.
func newAnycastReader(r *multicastReader) *anycastReader {

	reader := &anycastReader{reader: r}
	r.refCount += 1

	return reader
}

// doTopic implements a Reader interface method.
//
// Delegates to the multicastReader backend.
func (r *anycastReader) doTopic() *Topic {
	return r.reader.topic
}

// doChan implemnts a Reader interface method.
//
// Delegates to the multicastReader backend if the anycastReader hasn't been
// closed. If the anycastReader has been closed, then returns the global
// closedChannel.
func (r *anycastReader) doChan() chan *Message {

	if r.readerClosed {
		// return a closed channel instead of the shared channel
		return closedChannel
	}

	return r.reader.ch
}

// doClose implements a Reader interface method.
//
// Delegates to the multicastReader backend if the anycastReader hasn't been
// closed.
func (r *anycastReader) doClose() {

	if r.readerClosed {
		return
	}

	r.reader.doClose()
	r.readerClosed = true
}
